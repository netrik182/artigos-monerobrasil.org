---
title: "Behavioral Finance, Cryptocurrency Markets"
description: "Atualização do longa-metragem documental produzido e organizado por membros da comunidade Monero."
date: 2021-02-28T16:46:00-03:00
draft: false
tags:
  - vídeo
  -
categories:
  - todos
  - vídeos
  - notícias
  -
image: "behavioral-finance-cryptocurrency-markets.jpg"
author: ""
toc: false
---

# Monero: Sound Money, Safe Mode

Nessa palestra, a principal do TCV Summit de Fevereiro de 2021, Dr. Daniel Kim descorre sobre uma pesquisa premiada com estudos de caso dos mercados financeiros e criptomoedas para examinar o grau em que os mercados de criptomoedas atingem a descoberta eficiente de preços.

O palestra está disponível gratuitamente no YouTube:

{{< video "REC5V7d3pqM" >}}

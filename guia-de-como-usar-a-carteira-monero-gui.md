---
title: "Como usar a Carteira Monero GUI"
description: "Passo-a-passo de como usar a carteira oficial do Monero para Windows, Linux e MacOS"
date: 2021-01-02T19:56:08-03:00
draft: true
tags:
  - guia
  - carteira
  -
categories:
  - todos
  - guias
  - carteiras
  -
image: "thumbnail.svg"
author: ""
toc: true
---

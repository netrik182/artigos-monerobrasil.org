---
title: "Guia: Monero Node com Docker"
description: "Ensinando como rodar um nó Monero usando Docker"
date: 2021-11-09T01:00:00
draft: false
tags:
  - guia
  -
categories:
  - todos
  - guias
  -
image: "node-docker.png"
author: "lozbek"
toc: true
---

# Guia: Monero Node com Docker

## Introduçāo

Este é um guia adaptado para português brasileiro, originalmente escrito por [**_Seth for Privacy_**](https://blog.sethforprivacy.com/). A proposta desse guia é apenas facilitar a instalaçāo de um nó [**Monero**](https://www.getmonero.org/) utilizando Docker _container_.

Vale mencionar também que o guia a seguir foi baseado a partir de um computador Linux, rodando uma distribuiçāo baseada no [**Debian**](https://www.debian.org/). Sim, funciona no [**Ubuntu**](https://ubuntu.com/), [**Pop OS**](https://pop.system76.com/), [**Parrot OS**](https://www.parrotsec.org/), [**Kubuntu**](https://kubuntu.org/) e no [**Linux Mint**](https://linuxmint.com/). É possível [instalar o Docker no Windows](https://docs.docker.com/desktop/windows/install/) ou no [Mac](https://docs.docker.com/desktop/mac/install/), porém eu não pretendo abordar isso neste guia. Também é possível usar esse guia para instalar o node em uma [_Virtual Private Server_ (**VPN**)](https://www.pragmaticlinux.com/2020/07/setup-a-minimal-debian-10-buster-server-as-a-linode-vps/).

## Hardware necessário

- Para rodar o **node completo _(full node)_**, você precisa de pelo menos 2 _vCPU/cores_, 4GB de memória RAM e 180GB de espaço em disco (SSD recomendável).
- O **nó prunado _(prunned)_** precisa das mesmas especificaçōes (2 _vCPU/cores_ e 4GB de memória RAM), porém menos armazenamento: 80GB já basta.

{{< picture "community.png" "community.png" "Community Logo" >}}

## Motivação

A  rede [**Monero**](https://www.getmonero.org/) depende de uma rede distribuída de nós. Cada nó valida as transações, além de propagar transações para o resto da rede e ajudar os novos nós a se sincronizarem de forma fácil e rápida com o resto da rede. Rodar um nó oferece sólidas garantias de privacidade, aumenta a descentralização, estabilidade e velocidade da rede [**Monero**](https://www.getmonero.org/).

## Docker?

O [**_Docker_**](https://www.docker.com/) é uma plataforma de código aberto, desenvolvido na linguagem Go e criada pela empresa Docker.Inc. Basicamente, a plataforma garante maior facilidade na criação e administração de ambientes isolados, garantindo a rápida disponibilização de programas para o usuário final.

O [_Docker_](https://www.docker.com/) tem como objetivo criar, testar e implementar aplicações em um ambiente separado da máquina original, chamado de _container_. Dessa forma, o desenvolvedor consegue empacotar o software de maneira padronizada. Isso ocorre porque a plataforma disponibiliza funções básicas para sua execução, como: código, bibliotecas, _runtime_ e ferramentas do sistema.

{{< picture "docker.png" "docker.png" "Docker Logo" >}}

Imagina que o seu computador seria um barco apto para navegar e carregar coisas mundo afora. A proposta do [_Docker_](https://www.docker.com/) é acrescentar aplicações (caixas) prontas em cima do seu barco, sem que você precise se preocupar como instalar, manejar, atualizar e por aí vai.

A instalaçāo utilizando [Docker](https://www.docker.com/) tem uma série de beneficíos para o usuário, como manter o software automaticamente atualizado ([**Watchtower**](https://containrrr.dev/watchtower/)). simplicidade e carregamento automatico com o sistema operacional ([Debian Linux](http://www.debian.org), no nosso caso).

## Preparação

Antes de tudo, precisamos instalar alguns pacotes. Abra um terminal e digite:

```bash
sudo apt update && sudo apt-get upgrade -y
sudo apt install -y ufw curl
```

Agora nós instalamos o [Docker](https://www.docker.com/):

```bash
curl -fsSL https://get.docker.com -o get-docker.sh  
sudo sh get-docker.sh  
sudo usermod -aG docker $USER  
su - $USER
```

A próxima etapa agora é preparar as regras do firewall para permitir que o nó [Monero](https://www.getmonero.org/) consiga acessar a rede de forma segura e também permitir conexões via _SSH_.

```bash
sudo ufw default deny incoming  
sudo ufw default allow outgoing  
sudo ufw allow ssh  
sudo ufw allow 18080/tcp  
sudo ufw allow 18089/tcp  
sudo ufw enable
```

## Instalando Docker e rodando Monero

Sim, chegou a hora :) A instalação a seguir roda o _monerod (daemon)_ no _background_ e baixa a _blockchain_. Destaco aqui que o _container_ inclui apenas o monerod _(daemon)_, sendo que você precisa instalar a sua carteira _(wallet)_ de forma separada. Trata-se de uma decisão pessoal, eu recomendo a [**Feather Wallet**](https://featherwallet.org/) pela usabilidade e velocidade de carregamento.

Agora selecione qual implementação você deseja instalar, a versão completa do nó ou a prunada. São dois comandos e você deve copiar e colar um por vez.

**Blockchain completa (_full node_):**

```bash
docker run -d --restart unless-stopped --name="monerod" -p 18080:18080 -p 18089:18089 -v bitmonero:/home/monero sethsimmons/simple-monerod:latest --rpc-restricted-bind-ip=0.0.0.0 --rpc-restricted-bind-port=18089 --public-node --no-igd --no-zmq --enable-dns-blocklist
```

```bash
docker run -d \  
    --name watchtower --restart unless-stopped \  
    -v /var/run/docker.sock:/var/run/docker.sock \  
    containrrr/watchtower --cleanup \  
    monerod tor
```

**Blockchain prunada (_prunned_):**

```bash
docker run -d --restart unless-stopped --name="monerod" -p 18080:18080 -p 18089:18089 -v bitmonero:/home/monero sethsimmons/simple-monerod:latest --rpc-restricted-bind-ip=0.0.0.0 --rpc-restricted-bind-port=18089 --public-node --no-igd --no-zmq --enable-dns-blocklist --prune-blockchain
```

```bash
docker run -d \  
    --name watchtower --restart unless-stopped \  
    -v /var/run/docker.sock:/var/run/docker.sock \  
    containrrr/watchtower --cleanup \  
    monerod tor
```

Pronto, é só isso. Agora resta aguardar. Para acompanhar o _download_ dos blocos, apenas digite no terminal:

```bash
docker logs --follow monerod
```

{{< picture "console.jpg" "console.jpg" "Monerod Logs" >}}

## Atualizando o Node

Como estamos executando o [**Monero**](https://www.getmonero.org/) em um contêiner do [Docker](https://www.docker.com/) e implantamos o [**Watchtower**](https://containrrr.dev/watchtower/) junto com ele, o nó será reiniciado automaticamente com a versão mais recente do monerod sempre que uma nova versão for marcada no Git. Sim, você não precisa mais fazer o _update_ manualmente :)

## Tor (opcional)

Se expor sua porta RPC no [Tor](https://www.torproject.org/) como um serviço oculto, siga estes comandos.

```bash
sudo apt-get install tor  
sudo systemctl enable tor  
sudo mkdir /var/lib/tor/hidden_service  
sudo chown debian-tor:debian-tor /var/lib/tor/hidden_service
```

Agora você precisa abrir o arquivo torrc e adicionar as linhas abaixo no final. Sinta-se livre para usar qualquer editor de texto. No meu caso, eu escolhi o [_Gedit_](https://wiki.gnome.org/Apps/Gedit)  do [_Gnome_](https://www.gnome.org/).

```bash
sudo gedit /etc/tor/torrc
```

E adicione as linhas abaixo:

## Tor Monero RPC HiddenService

```bash
HiddenServiceDir /var/lib/tor/hidden_service/monero-rpc  
HiddenServicePort 18089 127.0.0.1:18089
```

Salve o arquivo e depois feche o editor de texto.

Chegou a hora de reiniciar o [**Tor**](https://support.torproject.org/apt/tor-deb-repo/) e visualizar qual o seu endereço .onion para acessar o seu nó. Execute os comandos abaixo:

```bash
sudo systemctl restart tor  
sudo cat /var/lib/tor/hidden_service/monero-rpc/hostname
```

O endereço .onion é o seu novo **_Monero RPC Hidden Service_**. Lembre-se também que é necessário usar a porta 18089 ao se conectar com o nó. Para testar a conectividade, basta abrir o [**Tor Browser**](https://www.torproject.org/) e acessar **_http: //substituiapeloenderecoonion:18089/get_info_** e verifique se você visualiza um bloco de texto (JSON).

## Usando o nó

Isso dependerá da carteira que você escolheu usar. Normalmente, o processo envolve apenas adicionar a especificação do endereço IP ou adicionar endereço Onion. Para acessar na mesma máquina, adicione **_127.0.0.1:18089 ou substituiapeloenderecoonion:18089._**

Aguarde a wallet sincronizar com os blocos e pronto, você está apto a utilizar [**Monero**](https://www.getmonero.org/) com absoluta soberania e privacidade.

{{< picture "feather-nodes.png" "feather-nodes.png" "Feather Node Tab" >}}

Na [**Feather Walle**t](https://featherwallet.org/faq/#how-can-i-connect-to-my-local-node), clique em File → Settings → Node → From custom list e adicione o seu endereço IP ou .onion seguido da porta 1809. Exemplo: **_127.0.0.1:1809_**

{{< picture "gui-docker.png" "gui-docker.png" "Monero GUI Node Tab" >}}

**Monero GUI Wallet**: selecione Settings → Node → Remote Node. Agora adicione o `.onion` address ou o seu endereço IP, seguido da porta. Selecione _Mark as Trusted Daemon_ também :)

Se houver dúvidas, veja esse [**_link_**](https://www.getmonero.org/pt-br/resources/user-guides/remote_node_gui.html) para verificar como se conectar a um nó remoto.

Espero ter ajudado. Saudaçōes, paz e um futuro próspero!

---
title: "Rodar um Nó Monero"
description: "Como e porquê rodar um nó Monero?"
date: 2021-01-03T20:37:00-03:00
draft: false
tags:
  - guia
  -
categories:
  - todos
  - guias
  -
image: "nodes.jpg"
author: "netrik182"
toc: true
---

## Introdução Básica

Existem vários motivos que podem levar uma pessoa a rodar um nó por conta própria ao invés de usar um nó remoto. E é importante conhecer um pouco das diferenças de cada um desses meios para saber qual opção é melhor você, considerando diferentes casos de uso e situações.

Antes de começarmos a falar propriamente do tema deste artigo, acredito que seja importante explicar, de forma simples, o que são "nós" e qual sua função.

Como muitos já sabem, a maior parte das [criptomoedas](/artigos/o-que-são-criptomoedas/) são descentralizadas e operam através de uma [rede p2p](https://pt.wikipedia.org/wiki/Peer-to-peer). Com o Monero não é diferente.

"Rodar um nó" nada mais é do que ter um computador (de uma pessoa ou servidor/empresa, por exemplo) que executa o software do Monero e compartilha com a rede P2P uma cópia da [blockchain](/artigos/blockchain/). Dessa forma, você está participando ativamente da rede Monero e ajudando todos os participantes na tarefa de verificar transações, propagá-las e manter a rede segura.

## Por que rodar meu próprio nó?

Rodar seu próprio nó garante que você tenha muito mais privacidade ao [conectar sua(s) carteira(s)](/artigos/como-criar-uma-conta-monerujo/#adicionar-um-n%C3%B3-remoto) a ele e também ajuda a aumentar ainda mais a estabilidade e velocidade da rede Monero, bem como sua descentralização.

## Pré-requisitos

- Obviamente, conexão com a internet
- CPU com 2 _cores_ ou mais
- 4 GB de RAM ou mais
- SSD com 50GB livres para um nó compacto (_pruned node_)
- SSD com 100GB livres para um nó complerto

## Como rodar um nó no Linux?

Assumindo que já tenha [baixado e verificado](/artigos/como-verificar-assinaturas/#3-baixar-e-verificar-o-arquivo-de-hashes) os arquivos do Monero, o próximo passo é criar o arquivo de configuração chamado `monerod.conf` com o seguinte conteúdo:

```bash
# Pasta onde a blockchain será salva. Substituir <user> pelo nome do seu usuário.
data-dir=/home/<user>/.bitmonero

# Logs do monerod. Substituir <user> pelo nome do seu usuário.
log-file=/home/<user>/.bitmonero/monerod.log
log-level=0

# Configurações de ip/porta e blocklist
no-igd=true
rpc-restricted-bind-ip=0.0.0.0
rpc-restricted-bind-port=18089
confirm-external-bind=true
enable-dns-blocklist=true
block-sync-size=10

# Remova o comando abaixo caso queira rodar um nó completo (80GB)
prune-blockchain=true
```

Após isso, salve o arquivo na mesma pasta que você baixou o software do Monero e execute o `monerod` indicando o arquivo de configuração:

```bash
#!/bin/bash
./monerod --config-file monerod.conf --detach
```

O parâmetro `--detach` irá executar o _daemon_ em segundo plano. Para acompanhar o andamento da sincronização você sempre poderá executar o comando `status` ou `help` para consultar a lista completa de opções, respectivamente:

```bash
./monerod status

./monerod help
```

## Como rodar um nó no Windows?

Assumindo que já tenha [baixado e verificado](/artigos/como-verificar-assinaturas/#3-baixar-e-verificar-o-arquivo-de-hashes) os arquivos do Monero, o próximo passo é criar o arquivo de configuração chamado `monerod.conf` com o seguinte conteúdo:

```cmd
# Pasta onde a blockchain será salva. Substituir <usuário> pelo nome do seu usuário.
data-dir=C:\Users\<usuário>\Downloads\Monero

# Logs do monerod. Substituir <usuário> pelo nome do seu usuário.
log-file=C:\Users\<usuário>\Downloads\Monero\monerod.log
log-level=0

# Configurações de ip/porta e blocklist
no-igd=true
rpc-restricted-bind-ip=0.0.0.0
rpc-restricted-bind-port=18089
confirm-external-bind=true
enable-dns-blocklist=true
block-sync-size=10

# Remova o comando abaixo caso queira rodar um nó completo (80GB)
prune-blockchain=true
```

Após isso, salve o arquivo na mesma pasta que você baixou o software do Monero e execute o `monerod` no terminal (cmd) indicando o arquivo de configuração:

```cmd
monerod.exe --config-file monerod.conf --detach
```

O parâmetro `--detach` irá executar o _daemon_ em segundo plano. Para acompanhar o andamento da sincronização você sempre poderá executar o comando `status` ou `help` para consultar a lista completa de opções, respectivamente:

```cmd
monerod.exe status

monerod.exe help
```




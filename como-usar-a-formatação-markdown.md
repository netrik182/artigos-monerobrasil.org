---
title: "Como usar a formatação Markdown"
description: "Um guia passo-a-passo de como formatar um texto usando Markdown"
date: 2020-05-18
draft: false
tags:
  - guia
  -
categories:
  - todos
  - guias
  -
image: "markdown.jpg"
author: ""
toc: false
---

Para inserir um título, basta usar o símbolo `(##)` (duas vezes) antes do título que for usar.

```txt
## Exemplo de título
```
## Exemplo de título

---

Para inserir um subtítulo, basta usar o símbolo `(###)` (3 vezes) antes do subtítulo que for usar.

```txt
### Exemplo de subtítulo
```
### Exemplo de subtítulo

---

Parágrafos não necessitam de nenhum tipo de formatação especial, devendo somente escrever o texto livremente. Deixe uma linha em branco entre um "bloco" de texto e outro quando for iniciar um novo parágrafo.

Nec ultrices dui sapien eget mi. Viverra suspendisse potenti nullam ac tortor. Faucibus in ornare quam viverra orci. Diam quam nulla porttitor massa. Sed risus ultricies tristique nulla aliquet enim tortor at. Tristique sollicitudin nibh sit amet commodo nulla facilisi nullam vehicula. At quis risus sed vulputate odio ut enim. Diam vel quam elementum pulvinar etiam non quam. Eget egestas purus viverra accumsan in nisl nisi. Nam libero justo laoreet sit amet cursus sit.

Nascetur ridiculus mus mauris vitae ultricies leo integer malesuada. Donec adipiscing tristique risus nec feugiat in. Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue. Leo duis ut diam quam nulla. Quam nulla porttitor massa id neque aliquam vestibulum morbi blandit. Sed risus pretium quam vulputate dignissim suspendisse in est ante.

---

Para inserir uma citação ou usar legenda em fotos, utilize o símbolo `(>)` antes do seu texto.

```txt
> Exemplo de citação
```
> Exemplo de citação

---

Também é possível criar uma lista de tópicos ao inserir um hífen `(-)` antes de cada item em questão. Veja o exemplo:

```txt
Lista de Tópicos

- Item 1
- Item 2
- Item 3
```

Lista de Tópicos

- Item 1
- Item 2
- Item 3

---

Ou então criar uma lista ordenada:

```txt
Lista Ordenada

1. Item 1
2. Item 2
3. Item 3
```

Lista Ordenada

1. Item 1
2. Item 2
3. Item 3

---

Para marcar um texto ou palavra em negrito utilize:

```txt
**Texto em negrito.**

Ou somente uma **palavra**.
```

**Texto em negrito.**

Ou somente uma **palavra**.

---

Para marcar um texto ou palavra em itálico utilize:

```txt
_Texto em itálico_ ou somente uma _palavra_.
```
_Texto em itálico_ ou somente uma _palavra_.

---

Ou tracejado:

```txt
~~Texto tracejado.~~

Ou somente uma ~~palavra~~.
```
~~Texto tracejado.~~

Ou somente uma ~~palavra~~.

---

Link são muito comuns em qualquer tipo de texto. Para adicionar links, basta usar os comandos `[texto](link)`, sem nenhum espaço entre os colchetes e parênteses, veja:

```txt
Página inicial do [Monero Brasil](https://monerobrasil.org)
```

Página inicial do [Monero Brasil](https://monerobrasil.org)

---

Tão importante quanto os links são as imagens. Para inseri-las é bem parecido com o link, porém usando um sinal de exclamação no início `![texto](link)`

```txt
![Imagem de colaboração](https://monerobrasil.org/images/todos.jpg)
```

![Imagem de colaboração](https://monerobrasil.org/images/todos.jpg)

---

Por fim, tabelas são criadas desta forma:

```txt
| Coluna 1 | Coluna 2 |
|----------|----------|
| Item 1   | Item 2   |
| Item 3   | Item 4   |
```
| Coluna 1 | Coluna 2 |
|--|--|
| Item 1 | Item 2 |
| Item 3 | Item 4 |

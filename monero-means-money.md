---
title: "Monero Means Money"
description: "Longa-metragem documental produzido e organizado por membros da comunidade Monero."
date: 2020-05-14T21:32:06-03:00
draft: false
tags:
  - vídeo
  -
categories:
  - todos
  - vídeos
  - notícias
  -
image: "monero-means-money.jpg"
author: ""
toc: false
---

# Monero Means Money

**Monero Means Money: Cryptocurrency 101, Live from Leipzig** é um longa-metragem documental cujo título em tradução literal seria "Monero Significa Dinheiro". A produção independente foi organizada por membros da comunidade e traz uma palestra do Dr. Daniel Kim em Leipzig sobre Bitcoin e a origem do Monero, explicando em detalhes o propósito desta última, uma criptomoeda fungível.

O documentário está disponível gratuitamente no YouTube e conta com legendas em português. Assista agora mesmo!

{{< video "8quGD9W7B2I" >}}

Para saber mais sobre **Monero Means Money**, visite a página [moneromeans.money](https://moneromeans.money).

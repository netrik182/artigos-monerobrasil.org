---
title: "Blockchain"
description: "O que é uma blockchain?"
date: 2021-01-03T18:21:00-03:00
draft: false
tags:
  - conceito
  -
categories:
  - todos
  - conceitos
  -
image: "blockchain.jpg"
author: ""
toc: false
---

## O que é uma blockchain?

O termo _blockchain_ refere-se a um método específico para armazenar, de forma segura, registros em um banco de dados compartilhado com todos os usuários da rede. Este método é inovador por ser um sistema _sem a necessidade de confiança_, onde os indivíduos possuem total autonomia sobre seus fundos (saldo), também onde não existe uma autoridade central e cada participante pode facilmente verificar e auditar o sistema.

Você pode pensar em "blockchains" como uma tecnologia que permite às redes estabelecerem acordos usando um "consenso distribuído". Ao possibilitar que desconhecidos compartilhem um registro de forma segura, torna-se possível o desenvolvimento de "criptomoedas" que funcionam como dinheiro digital. Existe uma infinidade de moedas (euros, dólares, reais, etc.); de forma análoga, diversas equipes desenvolveram diferentes criptomoedas (Monero, Ethereum, Bitcoin, etc.).

Uma vez que dados (ou transações) são gravados na blockchain, eles não podem mais ser deletados, movidos ou alterados de nenhuma forma. Os registros são imutáveis e cada participante da rede possui uma cópia correspondente da blockchain para verificarem por si próprios. A maioria das blockchains de criptomoedas empregam um modelo inteligente de mineração que encoraja a participação na rede e mantém todos os registros de forma honesta e sincronizados. Estes tipos de sistemas descentralizados são incrivelmente robustos, uma vez que não existe um servidor ou banco de dados central sequer que possa ser atacado ou manipulado de forma mal-intencionada.

> Este trecho foi extraído e adaptado do livro [Mastering Monero](https://netrik182.gitlab.io/docs-monerobrasil.org/masteringmonero/), disponível em português.

---
title: "Mastering Monero"
description: "O futuro das transações privadas. Traduzido para português."
date: 2020-10-31
draft: false
tags:
  - livro
  - tradução
  -
categories:
  - todos
  - livros
  -
image: "book-3.jpg"
author: "traduzidonetrik182"
toc: false
---

Mastering Monero é um livro que contém tudo o que você precisa saber para começar a usar Monero em seus negócios ou no dia-a-dia, mesmo que você ainda não tenha entendido ou interagido com criptomoedas anteriormente.

A versão disponível a seguir foi traduzia por netrik182 para português e você pode lê-la gratuitamente em: [Mastering Monero em português](https://netrik182.gitlab.io/docs-monerobrasil.org/masteringmonero/).

Todos os créditos são do autor original [SerHack](https://masteringmonero.com/).

---
title: "Pilares do Projeto Monero"
description: "O Monero é mais do que apenas uma tecnologia."
date: 2020-12-01T01:19:52-03:00
draft: false
tags:
  - conceito
  -
categories:
  - todos
  - conceitos
  -
image: "group.jpg"
author: ""
toc: true
---

## Pilares do Projeto Monero

O Monero é mais do que apenas uma tecnologia. Ele também é o que essa tecnologia representa. Abaixo estão listadas algumas das filosofias que orientam seu desenvolvimento.

### Segurança

Os usuários devem poder confiar suas transações no Monero, sem risco de erros ou ataques. O Monero dá a recompensa total dos blocos aos mineradores, que são os membros mais importantes da rede pois fornecem alta segurança. As transações são criptograficamente seguras, usando as melhores e mais avançadas ferramentas de criptografia disponíveis.

### Privacidade

O Monero leva a privacidade a sério. O Monero precisa ser capaz de defender seus usuários num tributal e, em casos extremos, até mesmo da pena de morte. Esse alto nível de privacidade deve estar completamente disponível a todos, sejam eles tecnologicamente competentes ou não tenham nenhuma ideia de como o Monero funciona. O usuário deve ser capaz de confiar fortemente no Monero, de maneira a não se sentir pressionado em mudar seus hábitos financeiros devido ao risco de ser rastreado e monitorado.

### Descentralização

O Monero se compromete em fornecer o máximo de descentralização possível. O Monero não é controlado por nenhum grupo específico e você não precisa confiar em nenhuma entidade da rede. O algoritmo "prova de trabalho" é acessível e permite a mineração do Monero através de computadores normais, o que dificulta que uma entidade adquira uma grande força de mineração. Os nós se conectam uns aos outros via I2P para reduzir os riscos de revelar informações confidenciais (em desenvolvimento). As decisões de desenvolvimento são extremamente claras e abertas à discussão pública. Os registros das reuniões dos desenvolvedores são publicados online em sua totalidade e são visíveis por todos.

*Artigo traduzido e adaptado de [getmonero.org](https://getmonero.org/).*

---
title: "Como instalar a Feather Wallet no Tails"
description: "Ensinando como instalar a Feather Wallet no Tails"
date: 2021-10-13T01:00:00
draft: false
tags:
  - guia
  - carteira
  -
categories:
  - todos
  - guias
  - carteiras
  -
image: "tails-feather.png"
author: "lozbek"
toc: false
---

## Como instalar a Feather Wallet no Tails

Antes de iniciar o guia, vale a pena explicar as vantagens e a proposta do [Tails](https://tails.boum.org/).

[Tails](https://tails.boum.org/) é a abreviação de _The Amnesic Incognito Live System_. Trata-se de um sistema operacional focado em segurança e privacidade, desenvolvido em Linux, destinado a ser executado a partir de um pendrive ou DVD.

A proposta do [Tails](https://tails.boum.org/) é não deixar rastros no sistema operacional original instalado no PC, seja na memória ou no sistema de arquivos. Uma das organizaçōes que financiam o Tails é o [Projeto Tor](https://www.torproject.org/), juntamente com a [Mozilla](https://www.mozilla.org), a [Freedom of the Press Foundation](https://freedom.press/) e o [Projeto Debian](https://www.debian.org/). 

Como um sistema operacional, o [Tails](https://tails.boum.org/) vem com vários aplicativos padrão, permitindo ao usuário navegar pela web de forma segura e anônima. O [Tails](https://tails.boum.org/) usa Tor por padrão para se conectar à Internet, o que significa que toda comunicação de saída é roteada pela rede Tor e conexões não anônimas não são permitidas. A seleção de pacotes inclui aplicativos para criptografar dados, gerador de senhas e ferramentas para minimizar o risco ao se conectar à Internet (todas as comunicações, incluindo e-mails e mensagens instantâneas, são criptografadas por padrão).
 
Até o momento, o [Tails](https://tails.boum.org/) não inclui uma _wallet_ Monero. Nós vamos corrigir isso, instalando a [Feather Wallet](https://featherwallet.org/) - uma carteira leve, rápida, fácil de usar e que ocupa menos de 40MB de espaço.

### Habilitando o módulo persistente

É altamente recomendado armazanr a sua carteira Monero no volume persistente do Tails. Se você ainda não o habilitou, clique em _Aplicativos → Configurar volume persistente_. Em seguida, digite e confirme sua senha e habilite o _Dotfiles_. Agora é só clicar _Save_.

{{< picture "dot-files.png" "dot-files.png" "Enable Dotfiles" >}}

### Instalando a Feather

- Faça o download da última versāo (`AppImage`) da Feather utilizando o website oficial [featherwallet.org/download](https://featherwallet.org/download).
- Mova o arquivo `.AppImage` para a pasta do volume Persistente se você deseja que a carteira permaneça salva.

{{< picture "move-feather.gif" "move-feather.gif" "Salve no Volume Persistente" >}}

- Torne o arquivo `.AppImage` executável.

{{< picture "executable-feather.gif" "executable-feather.gif" "Executável" >}}

- Clique duas vezes, abra a Feather Wallet.

> Se você for criar a sua carteira pela primeira vez, nāo se esqueça de anotar o backup das sementes (_seeds_) da sua carteira num papel :)

{{< picture "start-feather.gif" "start-feather.gif" "Iniciar Feather" >}}

A [Feather Wallet](https://featherwallet.org/) automaticamente conecta em um nó remoto. É possível utilizar o seu próprio node, basta clicar em _File → Settings → Node_ e adicionar o endereço do seu nó.

Na próxima vez que você iniciar o [Tails](https://tails.boum.org/), basta clicar 2x no arquivo `.AppImage` para abrir a [Feather Wallet](https://featherwallet.org/). Os dados da sua carteira serāo detectados automaticamente.

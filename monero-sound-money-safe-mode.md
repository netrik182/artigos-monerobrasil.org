---
title: "Monero: Sound Money, Safe Mode"
description: "Atualização do longa-metragem documental produzido e organizado por membros da comunidade Monero."
date: 2021-01-02T19:27:00-03:00
draft: false
tags:
  - vídeo
  -
categories:
  - todos
  - vídeos
  - notícias
  -
image: "monero-means-money-inverted.jpg"
author: ""
toc: false
---

# Monero: Sound Money, Safe Mode

Nesta atualização "pós-pandemia" do documentário **Monero Means Money**, Dr. Daniel Kim traz uma visão abrangente do Monero e do Bitcoin dentro de um contexto amplo da crise financeira de 2020, englobando o ouro e a moeda fiduciária. A apresentação foi filmada entre os dias 7 e 8 de agosto de 2020, durante a palestra principal da Vila do Monero na DEF CON 28.

O palestra está disponível gratuitamente no YouTube:

{{< video "aC9Uu5BUxII" >}}

Para saber mais sobre **Monero Means Money** como um todo, visite nosso outro post em [Monero Means Money](/artigos/monero-means-money/).
